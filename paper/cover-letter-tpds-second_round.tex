\documentclass[10pt,article]{article}
\usepackage[utf8]{inputenc}
\usepackage[american]{babel}

\usepackage{graphicx,xcolor}\graphicspath{{figures/}}
%\usepackage{subfigure}
\usepackage{subfig}
\usepackage{amssymb}
\usepackage{algorithm}
\usepackage{algorithmicx}
\usepackage[noend]{algpseudocode}

\usepackage{amsmath,amssymb,amsfonts,amsthm,bm}
\usepackage{xspace}
\usepackage{standalone}


\usepackage{amsrefs}
\usepackage{hyperref}

\usepackage{tikz}

\usetikzlibrary{patterns}
\usetikzlibrary{calc}
\usetikzlibrary{decorations.pathmorphing} % noisy shapes
\usetikzlibrary{fit}					% fitting shapes to coordinates
\usetikzlibrary{backgrounds}	% drawing the background after the foreground

\usetikzlibrary{arrows,shapes}
\usetikzlibrary{trees,snakes}
\usetikzlibrary{decorations.markings}
\usetikzlibrary{shadows}


\usepackage[framemethod=tikz]{mdframed}  % shaded 环境
\usepackage{framed}
\usepackage{array}
%\usepackage[ntheorem]{empheq}
%\usepackage[many]{tcolorbox}


\usepackage{ifthen}
\usepackage{todonotes}
\usepackage{color}
\usepackage{extarrows}
\usepackage{paralist}


\theoremstyle{plain}
\newtheorem{lemma}{Lemma}
\newtheorem{remark}{Remark}
\newtheorem{theorem}{Theorem}
\newtheorem{proposition}{Proposition}
\newtheorem{corollary}{Corollary}

\theoremstyle{definition}
\newtheorem{definition}{Definition}

%%Colorblind-friendly
\definecolor{pinkCB}{RGB}{255,109,182}
\definecolor{purpleCB}{RGB}{182,109,255}
\definecolor{yellowCB}{RGB}{255,255,109}
\definecolor{redCB}{RGB}{146,0,0}
\definecolor{blueCB}{RGB}{0,109,219}

\newcommand{\ema}[1]{\ensuremath{#1}\xspace}
\newcommand{\pfail}{\ema{p_{\text{fail}}}\xspace}



\begin{document}

\noindent
Dear Editor and Reviewers,~\\


Please find enclosed the second revised version of our IEEE TPDS
submission entitled \emph{Optimal Checkpointing Strategies
for Iterative Applications}.\\

We thank the reviewers for their comments on the original version and on the first revision. 
Several important additions have been made as a follow-up of their suggestions.
%First of all we would like to thank you for the feedback which we
%believe greatly helped to strengthen the paper. Specifically we believe
%that the idea to add evaluations that showed the robustness of the
%algorithm even in adversarial conditions of a non-deterministic
%performance was a really important item to the paper and to this we are
%grateful.
Please find below a list of all changes. \\

We hope that this second revised version is in suitable form for publication in IEEE TPDS. 
~\\
~\\
\noindent
Yves Robert (on behalf of all the authors)

\bigskip

\section{Reviewer 1}

\noindent
\textit{I'm concerned that that this work is more theoretical than practical.}\\

\noindent
\textbf{Answer}
Our work has a strong theoretical focus. It turns out that finding the optimal checkpointing pattern
for an iterative application requires a long and technically involved proof. 
As pointed out  in the document (sections 5.3.1. and 5.5), the practical interest of our
algorithm is indeed limited to specific
scenarios. However our practical contribution also lies
in the fact that we assess the other scenarios where one can safely rely on the
simpler greedy approaches à la Young/Daly. Finally, 
our optimal solution is provided handy to the user,
because our dynamic programming algorithm is made publicly available and one can easily use it to 
find the optimal pattern. When the checkpointing cost is low, or when there are few failures, the Young/Daly approach
is  good enough, but in all other situations,  our solution provides significant gain at (almost) no cost. In fact, the whole set of simulations shows that our approach is the only one that can guarantee the lowest overhead across
a wide range of scenarios.\\


\noindent
\textit{The checkpointing and recovery times seem to indicate that checkpointing synchronously to disk is used as opposed to checkpointing asynchronously to memory, SSD, or a buddy node as done in VeloC. With multi-level checkpointing becoming more common in large-scale HPC applications how does the faster checkpoints impact your results? Initially, I would think that if checkpointing and recovery is faster, then the overhead of doing checkpoint/recovery would decrease and the YD methods would perform better. Across the MTBFs, various Young Daly methods were within $<5\%$ of optimal and much closer to zero for current MTBFs. Even the results from Fig 12 show that CKPTYDAVE is only $1-2\%$ from optimal even for very high failure rates. Given that, do the authors see a system like this being used in practice?}\\


\noindent
\textbf{Answer}
Our approach is agnostic of the checkpoint protocol, we only need an estimate of the checkpoint time for each task.
And you are clearly right, the lower the checkpoint cost, the smaller the difference across all methods, because the
failure-free checkpoint overhead has less impact on performance. 
We have added a new set of experiments in the supplementary material, together with a new 
subsection in the main text. In these new experiments, we 
explore what happens when the checkpointing cost takes low values, owing to approaches such as VeloC.\\
%There remains the question of the importance of such algorithmic
%approach when the checkpointing cost decreases, or when there are very
%few errors. Here, Reviewer 1 is right: when there are very few failures,
%or when the checkpointing cost is negligible, developing efficient
%checkpointing strategies is less important because the general overhead
%due to the failure-related environment is less impactful. We have
%clarified this in the text.
%\todo[inline]{add a sentence on the figures where $\eta$ is varying both here and also add 
%a paragraph in the main text, 
%together with a citation to veloC here}



\noindent
\textit{The motivating example for a long running application is a matlab script that need 83 days. I assume this is on a single node which means the MTBF would be low and thus normal YD is just as efficient as these methods. If run on a larger system, parallelizing this task would decrease the runtime but require more resources. Does the decrease in execution time improve reliability more than adding more nodes decreases the MTBF?}\\

\noindent
\textbf{Answer}
The motivating example characterizes a neuroscience application running for 83 days on  a parallel platform.
Only the dynamic programming algorithm is written in Matlab.

For a perfectly parallel application, the number of processors does not matter, because the probability of a failure 
depends upon the product of the number of processors times the execution time: 
if the application runs in time $T_{p}= \frac{T_{seq}}{p}$ on $p$ processors,
then its probability of failure is $\pfail = 1 - e^{- (\lambda p) T_{p}} = 1 - e^{- \lambda T_{seq}}$, which is independent 
of the number of processors. In other words, regarding failures, 
an execution of, say, 5 minutes with 10,000 processors is the same as an execution
of 500 minutes with 100 processors. A nice feature of our approach and of the simulations is that we make full use of this
invariance: if a task runs for 5 minutes, then the task can use either 10 or 10,000 processors, the change is captured in the fault rate. 

For a parallel application obeying, say, Amdhal's law, the area equal to product of the number of processors times the execution time will increase with $p$, and so would the failure probability. But our approach holds without any change.\\


\noindent
\textit{In the GCR results in the appendix show that when the CCR is low CKPTYDAVG is near zero. For this algorithm what is an expected CCR, are the values 0.2,1, and 5 realistic? Also in the problem why are the same vectors recheckpointed for each task. If $p^0$ is saved for $a_0$ why checkpoint it again for all other tasks? Thus checkpointing times would improve.}\\

\noindent
\textbf{Answer}
Thanks for the suggestion, you are right, we can use incremental checkpointing for GCR. This decreases the checkpointing cost which remains constant. Now only the recovery cost is increasing for consecutive iterations. As for the CRR ratio,
we note that the operations are distributed level 1 BLAS routines, so it realistic that communication time is relatively close 
to communication time. We have extended the set of CCR values to 0.1, 0.2, 1, 5 and 10. Also, we
made our simulator publicly available so that the user can test their preferred parameter set.\\

%
%\noindent
%\textbf{Answer}

\noindent
\textit{Comments on the figures: The font is too small and is hard to read without using a PDF to zoom in. Figure 8 could be clipped to show just the import portion at the top.}\\


\noindent
\textbf{Answer}
We have updated the figures and corrected the typos. We have enlarged Figure 8 but did not clip it
 because we thought this 
could mislead a reader trying to quantify the total overhead.\\




\section{Reviewer 3}

\textit{But the authors still didn't address my second concern regarding parallel experimental setup. I believe the second reviewer also mentioned ``the contribution has no clear link with parallel and distributed systems.'', but the authors only addressed this by adding more wording motivation. Is it possible to add an experiment in a real parallel and distributed environment? Simulation result is not quite convincing.}\\

\noindent
\textbf{Answer}
%First of all we would like to thank you for the feedback which we
%believe greatly helped to strengthen the paper. Specifically we believe
%that the idea to add evaluations that showed the robustness of the
%algorithm even in adversarial conditions of a non-deterministic
%performance was a really important item to the paper and to this we are
%grateful.
As the reviewers have noted, this work is a theoretical work focusing on
the design of resilient algorithms. We agree that experiments in a real
parallel and distributed environment could considerably reinforce the
work, unfortunately we do not have the resources to do such an evaluation.
Currently, studying resilience in HPC systems where failures
occur once or twice every day requires system administrator rights, and the money to deploy
a large scale application.

Such an evaluation is itself a very important research
program that would complement our work but we cannot simply do it. 
Our paper lays the foundations for such an experimental evaluation, and we believe that 
our work will encourage practitioners to
test our new approach, owing to the public release of our code to compute the 
optimal checkpointing solution.



%\section{Reviewer 1}
%
%\noindent
%\textit{Although I found the use of theorems and proofs useful in showing optimal configurations. I found it difficult to read the middle sections without making a table of all the variables and terms. Adding a quick reference table would greatly aid the reader in being able to parse your paper's contributions. This paper contributes an algorithm that is now computable compared to other works such as reference 44. However, the authors do not present any timings on how the method performs or if its overhead is negates the benefits of their optimal period selection. Moreover, with respect to Table 1 are these results portable? How can we predict the optimal window without the times? Is it realistic to have these times a priori?}
%
%\noindent
%\textbf{Answer}
%\begin{compactitem}
%\item We have added a table for main notations (see Table 1)
%\item We have added a new section to report the execution time of the optimal dynamic programming algorithm for each application, together with a discussion. 
%%This 
%%execution time is small in front of the application time. For the neuroscience applications with 7 tasks, computing the optimal period requires less than one second as long as there is at most one failure every 100 iterations, and up to 50 seconds where there is at most one failure per 10,000 iterations.  The corresponding application times are using plain Matlab code, and could be easily divided
%%by a factor three or four using optimized C++ code. The neuroscience application runs for 
%%several days, so the overhead is negligible in practice.
%
%\item We have added new sections on the overhead and on the robustness of the method.
%Our approach takes the length and checkpoint size of each task as inputs. These parameters are known for many HPC applications such as linear algebra kernels and tensor operations (they can be acquired through either a performance model or through sampling). But following your suggestion, we have added a new experiment with
%stochastic task weights for the neuroscience application. This experiment confirms that an approximate knowledge of the parameters is enough to benefit from our approach.
%\end{compactitem}
%
%\bigskip
%\noindent
%\textit{The paper considers a communication-to-computation ratio to abstract their finding to different applications and situations. The selection of 2 and 5 seem arbitrary. Do these values match the patterns of the test applications?}
%
%\noindent
%\textbf{Answer}\\
%We have extended the range of communication-to-computation ratios for the GCR application
%and we now use $0.2$, $1$ and $5$
%in the experiments so as to cover the whole range of scenarios where the communication cost is either low (five times lower, $CCR=0.2$), balanced ($CCR=1$) or high (five times higher, $CCR=5$) in front of the computations
%(including our test applications). The results are similar to previous ones. Due to lack of space, the results have been moved to the Web supplementary material. 
%
%%\todo[inline]{It feels that we have not answered the reason why we chose those CCR ratios..}
%\bigskip
%\noindent
%\textit{In your comparison with Young/Daly, is your method of using Young/Daly valid? You assume you can only checkpoint once an iteration. However at higher failure rates, you might want to checkpoint faster than once an iteration. Thus, I feel your limitation may perturb your comparison for higher failure rates. What if you allow YD to checkpoint at anytime similar to your method, just the particular task is determined based on the YD interval? Do you still see the same improvement over YD?}
%
%\noindent
%\newcommand{\CYDave}{{\sc CkptYDAve}\xspace}
%\textbf{Answer}\\
%You are right, another possible extension of the Young/Daly approach is to work until the YD interval has been reached, and to checkpoint at the end of the current task (and repeat). A difficulty is that the checkpoint cost is not known in advance, hence the YD interval is not known either. A natural heuristic is to use the average of the checkpoint times over all tasks. We have implemented this new heuristic (called \CYDave), and now report its performance.
%Thanks for suggesting this! We note that other heuristics could use the minimum or the maximum checkpoint  instead of the average. For the record, we have implemented the three options and found that the outcome is very similar.
%
%\bigskip
%\noindent
%\textit{In Figure 7-9, the x axis is the probability of failing. It seems that you consider that each task has the same probability. Is this assumption reasonable? Wouldn't tasks that have higher execution times have higher probability of failing or those that used/depend of more system resources? The results in these figures show that most of the improvement due to your derivations occurs when the failure probability is high. Given this, what is a realistic probability of failure on current and future systems. Can you make the full relation to MTBF, which is more common in HPC papers? Would a large scale system like Summit or Fugaku be closer to the right or left hand side of your plots? The paper mentions multiple failures per iteration or failures every few iterations, but if iteration cost are low like for GCR, this seems to indicate that the MTBF is very low, much lower than current systems.}
%
%\noindent
%\textbf{Answer}\\
%Sorry for the confusion. The probability of failing is indeed related to the task execution time. 
%Specifically, with a fault rate $\lambda$ and execution time $t$, the probability for the task to fail 
%is $p = 1 - e^{-\lambda t}$. We have added a paragraph to explain this, and the relationship between 
%$\pfail$ (probability to fail within an iteration) and MTBF. We now report MTBF values
%for the neuroscience application. For the synthetic application, execution times are defined up to a constant (arbitrary factors), hence the usefulness of $\pfail$ values rather than absolute MTBF values.
%
%You are right when noting that the impact of our approach is less significant when the iteration length is small in front of the MTBF (or said differently, when there are very few failures). In that case, the granularity of the application is closer to the divisible model used for Young/Daly's formula where one can checkpoint any time; using this latter strategy is 
%then good enough. We have added elements of clarification on that specific point.
%%Now if we are dealing with a parallel iterative application whose tasks
%%execute on $N$ processors, the fault rate is $\lambda = N \lambda_{\mathit{ind}}$,
%%where $\lambda_{\mathit{ind}}$ is the individual fault rate of each processor. The relation to the platform MTBF
%%depends on $N$. We explicit this relationship in the text and give concrete examples.
%
%\bigskip
%\noindent
%\textit{
%- Page 3 you say ``To the best of our knowledge, this work is the first
%general-purpose approach to deal with fail-stop errors in
%iterative applications.'' This sentence should be removed or heavily revised as other common techniques such as redundancy or checkpointing in general apply to the latter part of that sentence. Therefore, I do not think you can claim you are the ``first''.\\
%- This paper considers fail-stop failures but does not make a clear argument why just checkpointing a single task is sufficient for recovery. Would this failure remove all data on the node not just that of the current running task? Thus, you are no longer able to recover? Adding a detailed description of what is the failure model of ``fail-stop'' task, process, node, system.}
%
%\noindent
%\textbf{Answer}\\
%We have modified the statement on `the first general-purpose approach'. And we have added a detailed description
%of the impact of `fail-stop' errors on the platform.
%
%
%\section{Reviewer: 2}
%
%\noindent
%\textit{In terms of motivation, the paper makes unrealistic assumptions. If the MTBF of a component is 10 years
%and we need 100K components to build a supercomputer, then probably better / fewer components are needed so
%we don't end up with an MTBF less than one hour. Who would ever buy such a machine (and hire a guy to run
%around every half an hour to replace components)? Try to find other unexpected events that need checkpointing.
%Similarly, in Sec 3.2. the downtime is presented as ``time to replace a component". Nobody keeps the whole machine
%idle until a component is replaced - imagine everybody yelling at the guy responsible for replacing
%components to move faster). In real life, the downtime is the time needed to start a new job that can restart
%(if the original job didn't already run with enough spare nodes).}
%
%\noindent
%\textbf{Answer}\\
%As mentioned above, we have added a detailed description
%of the impact of fail-stop errors on the platform. The downtime is usually the time to migrate to a spare,
%we have updated the text and provided references.
%
%\bigskip
%\noindent
%\textit{While the paper makes a point about failures at scale, the contribution has no clear link with parallel and distributed
%systems. The authors simply consider a serial execution of tasks. It does not matter whether a task is a single process
%or multiple processes. There are no parallelization aspects that bring any checkpointing challenges. Try to strengthen
%this link.}
%
%\noindent
%\textbf{Answer}\\
%We have added a new paragraph to better motivate this work for HPC computing.
%
%\bigskip
%\noindent
%\textit{Occasionally, the paper goes a bit too far with the mathematical notations, either
%introducing unnecessary clutter (e.g. Def 4: there is no need for a formal definition for a periodic schedule,
%this is quite obvious and already illustrated in Fig 2) or relying too much on them in the explanations (e.g. Sec 4:
%try to simplify this and explain it more intuitively). Also, try to motivate your approach. For example, you introduce
%the notion of checkpoint paths and patterns, but it's not clear where all this is going until everything comes together
%much later. Try to start with an intuitive overview of the proof and the reasoning behind the building blocks (you already have figures
%and examples that can be used earlier), the  move on to the formal aspects. Also, the algorithmic description in Sec 4.4.2
%deserves more consideration, as it is the core part of the contribution. An example would go a long way.}
%
%\noindent
%\textbf{Answer}\\
%We added an overview of the proof at the beginning of Section 4. As mentioned above, we added a table of main notations to help the reader follow the proof. The core of the algorithm (dynamic program) is illustrated in Figure 6.
%
%
%\bigskip
%\noindent
%\textit{The experiments need further refinement. Since these are simulations, the authors can easily expand the depth and scope of
%the study. While they use three applications and compare with three other approaches (which are credible), the figures are
%barely readable and focus on metrics that are hard to understand. Instead of \pfail values and normalized makespan,
%use metrics that are relevant in real life: measure a baseline run without failures (in real life we don't run infinite applications),
%then add the four strategies and measure their runtime for various failure rates (expressed as MTBF). Comment on the results and
%implications: how much time is lost due to an inefficient strategy, how does that relate to the baseline (does it make a big difference in percent?)
%Also, scalability experiments would be interesting to add (i.e. fixed failure rate, increasing number of tasks).}
%
%\noindent
%\textbf{Answer}\\
%We have also updated the presentation of the figures to make them more readable.
%We have added values for baseline execution times, absolute overheads and MTBFs as additional information. We have also added values for the execution time of the optimal dynamic programming algorithm. Finally, we have  added an experiment on  scalability by varying the number of iterations in the neuroscience application.  
%However we have chosen to keep most of the plots as a function of $\pfail$, which
%models a relation between the length of an iteration and the MTBF. In this
%context, the absolute value of the MTBF makes less sense. 
%As mentioned above, when the MTBF is very
%large \emph{in front of} the length of an iteration, 
%the application behaves as a divisible workload; in this case, 
%the Young/Daly strategy is efficient and
%sufficient (this is what we observe for $\pfail=10^{-3}$). If we plot the
%results as a function of a MTBF, this result gets less visible.
%
%\clearpage
%\section{Reviewer: 3}
%
%
%
%\noindent
%\textit{Overall, I think the paper provides values to HPC resilience research, especially checkpointing/restart techniques, but I do have a heavy concern about the experiments conducted in the work to demonstrate the superiority of the proposed strategies. Specifically,\\
%- Although there is a theoretical analysis of the dynamic-programming algorithm that computes the optimal period, there is no time overhead reported in the evaluation. Will this cost become a heavy overhead in practice?
%- The authors claimed the existing works were proposed for specific iterative algorithms, while work is designed for general purpose. However, the tested applications are still Krylov subspace method GCR and a synthetic application. This really weakens the contribution of this paper. I believe all the readers prefer to see a real-world iterative application with different time costs for different tasks and iterations.
%- There is no overhead reported for the failure-free case.}
%
%\noindent
%\textbf{Answer}\\
%As stated above, we have added values for baseline execution times, absolute overheads and MTBFs. 
%
%
%\bigskip
%\noindent
%\textit{It seems the work has little relevant and discussion about parallel and distributed system, is there any difference when using the proposed strategies in the real HPC systems? For example, how to perform the dynamic-programming algorithm considering the execution time of the same task and iteration may vary across different processes and nodes?}
%
%\noindent
%\textbf{Answer}\\
%Following your recommandation, we have extended our analysis of the neuroscience application to stochastic task execution times (sec 5.2.4). This was possible thanks to a modelization made by the authors of [20]. This stochasticity can represent the variation due to different executions for different task data (as was the case in [20]), or may come from variations of  the node/process computational speed. 
%There we have assumed that the dynamic program uses the expected value for each task, and we observed that the performance was quite robust.

\end{document}
